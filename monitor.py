from collections import defaultdict
from datetime import datetime, timedelta
from typing import List

import pytz
import requests
import dateutil.parser

OUTDATED_HOURS = 72
REPORTS_TO_CHECK_PER_LAYER = 10

installations = [
    {
        'name': 'basisbeveiliging.nl',
        'address': 'https://basisbeveiliging.nl',
        'alert_mails': ['info@faalkaart.nl'],
    },
]


def alert(installation, message):
    print(f"ALERT: {installation['name']}: {message}.")


def test_installation_for_fresh_data(installation):

    # get the map data, so there are visible organisations
    country_and_layers = get(installation, "/data/short_and_simple_stats/0/")
    if not country_and_layers:
        return

    # outdated contains the ratings that are obsolete. This is a total for the whole installation.
    outdated = []
    good = []
    for co_index, country in enumerate(country_and_layers):
        for la_index, layer in enumerate(country_and_layers[country]):
            if country_and_layers[country][layer]['organizations'] == 0:
                continue

            map_data =  get(installation, f"/data/map/{country}/{layer}/0//")
            if not map_data:
                alert(installation, "could not retrieve map data")

            for ma_index, feature in enumerate(map_data['features']):

                # three reports is a nice representation.
                if ma_index > REPORTS_TO_CHECK_PER_LAYER:
                    continue

                properties = feature['properties']
                organization_id = properties['organization_id']

                latest_report = get(installation, f"/data/report/{country}/{layer}/{organization_id}/0")
                out, go = get_outdated_scans(latest_report, OUTDATED_HOURS)
                outdated += out
                good += go

    if outdated:
        alert(installation, create_outdated_report(installation, outdated, good))


def get_outdated_scans(report, hours: int = OUTDATED_HOURS):
    now = datetime.now(pytz.utc)
    a_while_ago = now - timedelta(hours=hours)

    infractions = []
    good = []
    for url in report['calculation']['organization']['urls']:
        for endpoint in url['endpoints']:
            for rating in endpoint['ratings']:
                if dateutil.parser.isoparse(rating['last_scan']) < a_while_ago:
                    infractions.append(rating)
                else:
                    good.append(rating)

    return infractions, good


def get(installation, path):
    url = f"{installation['address']}{path}"
    print(f"Getting: {url}")
    response = requests.get(url)
    return response.json()


def create_outdated_report(installation, outdated: List, good: List):
    failure_rate = round((len(outdated) / (len(outdated) + len(good))) * 100, 2)
    good_rate = round((len(good) / (len(outdated) + len(good))) * 100, 2)

    report = f"Outdated report on {installation['name']}\n\n"

    report += f"Scans are outdated after: {OUTDATED_HOURS} hours.\n"
    report += f"Reports checked per layer: {REPORTS_TO_CHECK_PER_LAYER}. \n\n"

    report += f"Good rate: {good_rate}% ({len(good)})\n"
    report += f"Failure rate: {failure_rate}% ({len(outdated)})\n\n"

    failures_per_scan_type = defaultdict(dict)

    print(outdated)

    for outdate in outdated:
        if not failures_per_scan_type[outdate.get('scan_type', 'unknown')]:
            failures_per_scan_type[outdate.get('scan_type', 'unknown')] = {'total': 0, 'outdated': 0, 'good': 0}

        failures_per_scan_type[outdate.get('scan_type', 'unknown')]['total'] += 1
        failures_per_scan_type[outdate.get('scan_type', 'unknown')]['outdated'] += 1

    for outdate in good:
        if not failures_per_scan_type[outdate.get('scan_type', 'unknown')]:
            failures_per_scan_type[outdate.get('scan_type', 'unknown')] = {'total': 0, 'outdated': 0, 'good': 0}

        failures_per_scan_type[outdate.get('scan_type', 'unknown')]['total'] += 1
        failures_per_scan_type[outdate.get('scan_type', 'unknown')]['good'] += 1

    for key in failures_per_scan_type.keys():
        scan_failure_rate = round(failures_per_scan_type[key]['outdated'] / (failures_per_scan_type[key]['good'] + failures_per_scan_type[key]['outdated']) * 100, 2)
        scan_good_rate = round(failures_per_scan_type[key]['good'] / (failures_per_scan_type[key]['good'] + failures_per_scan_type[key]['outdated']) * 100, 2)
        report += f"{key.rjust(50, ' ')}: {scan_good_rate}% ({failures_per_scan_type[key]['good']}) good, " \
                  f" {scan_failure_rate}% ({failures_per_scan_type[key]['outdated']}) outdated.\n"

    return report


if __name__ == "__main__":
    for installation in installations:
        test_installation_for_fresh_data(installation)